﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeatureParams
{
    public class SortingParam
    {
        public static readonly Tuple<string, string> SortSettings = new Tuple<string, string>("Asc", "Desc");
        public static readonly Tuple<string, string> OrderBySettings = new Tuple<string, string>("Name", "Abbreviation");

        public string Sort { get; set; } = SortSettings.Item1;
        public string OrderBy { get; set; } = OrderBySettings.Item1;
    }
}
