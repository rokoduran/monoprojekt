﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeatureParams
{
    public class FilteringParam
    {
        public string NameFilter { get; set; } = "";
        public string AbbrvFilter { get; set; } = "";
        //Special filter for filtering models by make.
        public string MakeNameFilter { get; set; } = "";
    }
}
