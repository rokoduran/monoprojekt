﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeatureParams
{
    public class PagingParam
    {
        public static int PageSize = 10;
        public int currentPage { get; set; }
        public int totalPages { get; set; }
    }
}
