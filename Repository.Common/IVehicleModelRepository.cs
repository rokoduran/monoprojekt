﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeatureParams;
using Model.Common;

namespace Repository.Common
{
    public interface IVehicleModelRepository: IGenericRepository<IVehicleModel>
    {
    }
}
