﻿using FeatureParams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Common
{
    //Generic repository pattern.
    //Generic interface repo for all CRUD operations
    public interface IGenericRepository<TType> where TType : class
    {
        Task<TType> GetByIDAsync(int id);
        Task<int> GetCountAsync(FilteringParam filter = null);
        Task<IEnumerable<TType>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort);
        Task<TType> InsertAsync(TType insert);
        Task<TType> UpdateAsync(TType update);
        Task<bool> DeleteAsync(int id);

        Task SaveChangesAsync();
    }
}
