﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Model.Common;

namespace DataAccessLayer.DatabaseModels
{
    public class VehicleModelEntity: IBase
    {
        [Required, StringLength(40)] public string Name { get; set; }
        [Required, StringLength(10)] public string Abbreviation { get; set; }

        [Required] public int MakeId { get; set; }
        public VehicleMakeEntity Make { get; set; }

        [ScaffoldColumn(false)] public int ID { get; set; }

        [Required] public DateTime DateCreated { get; set; }
        [Required] public DateTime DateUpdated { get; set; }
    }
}
