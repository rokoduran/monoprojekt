﻿using DataAccessLayer.DatabaseModels;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Context
{
    public class VehicleContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=SHAENLIR\\SQLEXPRESS;Database=MonoDB;Trusted_Connection=True;");
        }
        public DbSet<VehicleMakeEntity> VehicleMakes { get; set; }
        public DbSet<VehicleModelEntity> VehicleModels { get; set; }
    }
}
