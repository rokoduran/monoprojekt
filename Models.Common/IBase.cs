﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.Common
{
    public interface IBase
    {
        [ScaffoldColumn(false)] int ID { get; set; }

        [Required] DateTime DateCreated { get; set; }
        [Required] DateTime DateUpdated { get; set; }
    }
}
