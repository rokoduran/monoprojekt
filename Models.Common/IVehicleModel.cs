﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.Common
{
    public interface IVehicleModel: IBase
    {
        [Required, StringLength(40)] string Name { get; set; }
        [Required, StringLength(10)] string Abbreviation { get; set; }

        [Required] int MakeId { get; set; }
        IVehicleMake Make { get; set; }
    }
}
