﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Common
{
    public interface IVehicleModelService: IGenericService<IVehicleModel>
    {
    }
}
