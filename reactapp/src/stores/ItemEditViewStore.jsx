import { makeAutoObservable, autorun, runInAction } from "mobx"

export class ItemEditViewStore{
    promptBoolean
    constructor(){
        makeAutoObservable(this)
        this.promptBoolean = false;
    }
    triggerPrompt(){
        this.promptBoolean = !this.promptBoolean;
    }
}