import { makeAutoObservable, observable, autorun, runInAction } from "mobx"
import CommunicationLayer from '../common/services/CommunicationLayer'
import FilterData from '../common/utilities/FilterData'
import PagingData from '../common/utilities/PagingData'
import SortingData from '../common/utilities/SortingData'
import Model, { ModelData } from './ModelStore'

export class MakeStore{
    paramsData;
    communicationLayer;
    makeData;
    newModelData;
    makeList;
    filterData;
    pagingData;
    sortingData;
    constructor(){
        makeAutoObservable(this);
        this.communicationLayer = new CommunicationLayer('make');
        this.makeData = new MakeData(this);
        this.newModelData = new ModelData(this);
        this.filterData = new FilterData(this);
        this.pagingData = new PagingData(this);
        this.sortingData = new SortingData(this);
        console.log(this.sortingData);
        this.paramsData = {
            "param1": this.filterData.dataAsJson(),
            "param2": this.sortingData.dataAsJson(),
            "param3": this.pagingData.dataAsJson()
        }
        this.makeList = [];
    }

    filterMakes(filterToFill, value){
        this.filterData.fillFilter(filterToFill, value);
        this.pagingData.setPagingData(1);
    }

    submitNewMake(){
        this.communicationLayer.create(this.makeData);
    }

    updateMake(){
        this.communicationLayer.update(this.makeData);
    }

    submitNewModelForMake(){
        this.communicationLayer.create(this.newModelData, 'model');
    }

    makeIndex(){
        this.paramsData = {
            "param1": this.state.filterData,
            "param2": this.state.orderData,
            "param3": this.state.pagingData
        }
        let responseData = this.communicationLayer.index(this.paramsData);
        this.makeList = [...responseData.data.makeData];
        this.pagingData.setPagingData(responseData.data.pagingInfo.currentPage, responseData.data.pagingInfo.totalPages);   
    }
}

export class MakeData{
    parent;
    id;
    name;
    abbreviation;

    constructor(parent){
        this.parent = parent;
        this.reset();
    }

    reset(){
        this.id = 0;
        this.name = '';
        this.abbreviation = '';
    }

    get makeDataAsJson(){
        return {
            id : this.id,
            name: this.name,
            abbreviation: this.abbreviation,
        }
    }

    setName(value){
        this.name = value;
    }

    setAbbv(value){
        this.abbreviation = value;
    }
}

export default MakeStoreImpl = new MakeStore();