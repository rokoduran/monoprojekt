import { makeAutoObservable, autorun, runInAction } from "mobx"
import FilterData from '../common/utilities/FilterData'
import PagingData from '../common/utilities/PagingData'
import SortingData from '../common/utilities/SortingData'
import CommunicationLayer from '../common/services/CommunicationLayer'

export class ModelStore{
    paramsData;
    communicationLayer;
    modelData;
    modelList;
    filterData;
    pagingData;
    sortingData;
    constructor(){
        makeAutoObservable(this);
        this.communicationLayer = new CommunicationLayer('model');
        this.modelData = new ModelData(this);
        this.filterData = new FilterData(this);
        this.pagingData = new PagingData(this);
        this.sortingData = new SortingData(this);
        this.paramsData = {
            "param1": this.state.filterData,
            "param2": this.state.orderData,
            "param3": this.state.pagingData
        }
        this.modelList = [];
    }

    filterModels(filterToFill, value){
        this.filterData.fillFilter(filterToFill, value);
        this.pagingData.setPagingData(1);
    }

    updateModel(){
        this.communicationLayer.update(this.modelData);
    }

    modelIndex(){
        this.paramsData = {
            "param1": this.filterData.dataAsJson(),
            "param2": this.orderData.dataAsJson(),
            "param3": this.pagingData.dataAsJson()
        }
        let responseData = this.communicationLayer.index(this.paramsData);
        this.modelList = [...responseData.data.modelData];
        this.pagingData.setPagingData(responseData.data.pagingInfo.currentPage, responseData.data.pagingInfo.totalPages); 
    }

    getModelList(){
        return this.modelList;
    }
}

export class ModelData{
    parent;
    id;
    name;
    abbreviation;
    makeId;

    constructor(parent){
        this.parent = parent;
        this.reset();
    }
    reset(){
        this.id = 0;
        this.name = '';
        this.abbreviation = '';
        this.makeId = 0;
    }

    get newModelDataAsJson(){
        return {
            id : this.id,
            name: this.name,
            abbreviation: this.abbreviation,
            makeId: this.makeid
        }
    }

    get editModelDataAsJson(){
        return {
            id : this.id,
            name: this.name,
            abbreviation: this.abbreviation,
        }
    }

    setName(value){
        this.name = value;
    }

    setAbbv(value){
        this.abbreviation = value;
    }

    setMakeId(value){
        this.makeid = value;
    }
}