import { makeAutoObservable, autorun, runInAction } from "mobx"

export class FilterData{
    nameFilter
    abbrvFilter
    makeNameFilter
    parentStore = null
    constructor(parent){
        makeAutoObservable(this)
        this.parentStore = parent;
        this.clearFilters();
    }

    dataAsJson(){
        return {
            nameFilter: this.nameFilter,
            abbrvFilter: this.abbrvFilter,
            makeNameFilter: this.makeNameFilter
        }
    }

    fillFilter(filterToFill, value){
        switch(filterToFill){
            case "nameFilter":
                this.nameFilter = value;
                break;
            case "abbrvFilter":
                this.abbrvFilter = value;
                break;
            case "makeNameFilter":
                this.makeNameFilter = value;
                break;
            default: return null;
        }
    }
    
    clearFilters(){
        this.nameFilter = ''
        this.abbrvFilter = ''
        this.makeNameFilter = ''
    }

}

export default FilterData;