import { makeAutoObservable, autorun, runInAction } from "mobx"

export class SortingData{
    parentStore;
    sort
    orderBy
    constructor(parent){
        makeAutoObservable(this)
        this.parentStore = parent;
        this.reset();
    }

    dataAsJson(){
        return {
            sort: this.sort,
            orderBy: this.orderBy
        }
    }

    setSort(newSort){
        this.sort = newSort;
    }

    setOrderBy(newOrder){
        this.orderBy = newOrder;
    }

    reset(){
        this.sort = 'Asc';
        this.orderBy = 'Name';
    }
}

export default SortingData;