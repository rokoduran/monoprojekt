import { makeAutoObservable, autorun, runInAction } from "mobx"

export class PagingData{
    parentStore;
    currentPage;
    totalPages;
    constructor(parent){
        makeAutoObservable(this)
        this.parentStore = parent;
        this.reset();
    }

    dataAsJson(){
        return{
            currentPage: this.currentPage,
            totalPages: this.totalPages
        }
    }

    setPagingData(newCurrentPage, newTotalPages = null){
        this.currentPage = newCurrentPage;
        if(newTotalPages !=null){
            this.totalPages = newTotalPages;
        }
    }

    reset(){
        this.currentPage = 1;
        this.totalPages = 1;
    }
}

export default PagingData