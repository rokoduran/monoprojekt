import axios from 'axios';

var requestUrl = 'https://localhost:44321/api/'


export class CommunicationLayer{
    requestURL
    controller
    constructor(controllerName, URL = requestUrl){
        this.controller = controllerName;
        this.requestURL = URL;
    }
    async index(data){
        await axios.post(requestUrl + this.controller + '/index', data).then(response => {
            return response;
        })
        .catch(err =>{
            console.log(err);
        })

    }

    async create(data, controllerName = this.controller) {
        await axios.post(requestUrl + controllerName + '/create', data).then(response => {
            return response;
        })
        .catch(err =>{
            console.log(err)
        });
    };

    async update(data) {
        await axios.put(requestUrl + this.controller + '/update', data).then(response => {
            return response;
        })
        .catch(err =>{
            console.log(err)
        });
    };

    async delete(id) {
        await axios.post(requestUrl + this.controller + '/delete/'+ id).then(response => {
            return response;
        })
        .catch(err =>{
            console.log(err)
        });
    };
}

export default CommunicationLayer;