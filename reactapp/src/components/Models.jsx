import React, { Component } from 'react';
import axios from 'axios';
import { Modal, ModalHeader, ModalBody, ModalFooter, Table, Button, FormGroup, Label, Input, ButtonGroup } from 'reactstrap';

//First version of react frontend without mobx.
class Models extends Component {
    state = {  
        responseData: null,
        editModelPrompt: false,
        
        pagingData: {
          currentPage: 1,
          totalPages: 1
        },

        filterData:{
          nameFilter : '',
          abbrvFilter:'',
          makeNameFilter: ''
        },

        orderData:{
          sort: 'Asc',
          orderBy: 'Name'
        },

        modelData:{
            id: 0,
            name: '',
            abbreviation: '',
        },
    }


    toggleEditModelPrompt(id){
        let data = this.state.modelData;
        data.id = id;
        this.setState({modelData: data, editModelPrompt:!this.state.editModelPrompt});
    }

    changePage(newPageNumber){
      let page = this.state.pagingData;
      page.currentPage = newPageNumber;
      this.setState({pagingData: page});
      this.updateModelList();
    }

    async fillNameFilter(value){
      let filter = this.state.filterData;
      filter.nameFilter = value;
      this.setState({filterData: filter});
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateModelList();
      }
    }

    async fillAbbvFilter(value){
      let filter = this.state.filterData;
      filter.abbrvFilter = value;
      this.setState({filterData: filter});
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateModelList();
      }
    }

    async fillMakeNameFilter(value){
        let filter = this.state.filterData;
        filter.makeNameFilter = value;
        this.setState({filterData: filter});
        if(this.state.pagingData.currentPage!=1){
          this.changePage(1);
        }
        else{
          this.updateModelList();
        }
      }

    async setOrderSelected(value){
      let order = this.state.orderData;
      order.orderBy = value;
      this.setState({orderData: order });
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateModelList();
      }
    }
    
    async setSortSelected(value){
      let sort = this.state.orderData;
      sort.sort = value;
      this.setState({orderData: sort});
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateModelList();
      }
    }

    async updateModelList(){
      let data = {
          "param1": this.state.filterData,
          "param2": this.state.orderData,
          "param3": this.state.pagingData
      }
        await axios.post('https://localhost:44321/api/model/index', data).then(response => {
            this.setState({responseData: response});
            let paging = this.state.pagingData;
            paging.totalPages = response.data.pagingInfo.totalPages;
            paging.currentPage = response.data.pagingInfo.currentPage;
            this.setState({pagingData: paging});
        })
        .catch(err => {
            console.log(err);
        });
    }

    async submitModel() {
        await axios.put('https://localhost:44321/api/model/update', this.state.modelData).then(response => {
            this.toggleEditModelPrompt(0);
        });
        this.updateModelList();

    };

    async submitDeletionModel(modelId){
        await axios.delete('https://localhost:44321/api/model/delete/' + modelId).then(response =>{
            this.updateModelList();
        });
    };

    componentDidMount() {
        this._asyncRequest = () => {
            this._asyncRequest = null;
        };
        this.updateModelList();
      }

    render() { 
        if(this.state.responseData == null){
            return(<div className="container">Fetching data...</div>);
        }
        else{
            let modelList =this.state.responseData.data.modelData.map(model => {
                return(              
                    <tr key={model.id}>
                      <td>{model.name}</td>
                      <td>{model.abbreviation}</td>
                      <td>{model.makeName}</td>
                      <td>
                        <Button color="success" size="sm" className="mx-1" onClick={this.toggleEditModelPrompt.bind(this,model.id)}>Edit</Button>
                        <Button color="danger" size="sm" className="mx-1" onClick={this.submitDeletionModel.bind(this, model.id)}>Delete</Button>
                      </td>
                    </tr>
                  )
            })
            let pagesButtons = [];
            for(let i = 0; i < this.state.pagingData.totalPages; i++){
              pagesButtons.push(
                <Button key={i+1} color="secondary" onClick={this.changePage.bind(this,i+1)} active={this.state.pagingData.currentPage === i+1}>{i+1}</Button>
              )
            }
        
    return (  
    <div className="App container">  
          <div>
            <Modal isOpen = {this.state.editModelPrompt} toggle={this.toggleEditModelPrompt.bind(this, 0)}>
              <ModalHeader toggle={this.toggleEditModelPrompt.bind(this)}>Editing model</ModalHeader>
              <ModalBody>

                <FormGroup>
                  <Label for="editModelName">Name</Label>
                  <Input type="text" id="editModelName" onChange={(ele) => {
                    this.state.modelData.name = ele.target.value;
                    }
                  } />
                </FormGroup>                
                <FormGroup>
                  <Label for="editMakeAbbreviation">Abbreviation</Label>
                  <Input type="text" id="editMakeAbbreviation" onChange={(ele) => {
                    this.state.modelData.abbreviation = ele.target.value;
                    }} />
                </FormGroup>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.submitModel.bind(this)}>Submit</Button>{' '}
                <Button color="secondary" onClick={this.toggleEditModelPrompt.bind(this,0)}>Cancel</Button>
              </ModalFooter>
            </Modal>
          </div>
        <Table striped bordered hover variant="dark" size="sm">
          <thead variant="dark">
              <tr key="searchParam">
                <td>Search by name</td>
                <td>Search by abbreviation</td>
                <td>Search by Make name</td>
                <td>Order by</td>
                <td>Sort type</td>
              </tr>
              <tr key="searchBoxes">
                <td>
                  <Input type="text" id="searchNameInput" onChange = {(ele) => this.fillNameFilter(ele.target.value)}/>
                </td>
                <td>
                  <Input type="text" id="searchAbbvInput" onChange = {(ele) => this.fillAbbvFilter(ele.target.value)}/>
                </td>
                <td>
                  <Input type="text" id="searchMakeNameInput" onChange = {(ele) => this.fillMakeNameFilter(ele.target.value)}/>
                </td>
                <td>
                  <ButtonGroup>
                    <Button color="primary" onClick={this.setOrderSelected.bind(this,'Name')} active={this.state.orderData.orderBy === 'Name'}>Name</Button>
                    <Button color="primary" onClick={this.setOrderSelected.bind(this,'Abbreviation')} active={this.state.orderData.orderBy === 'Abbreviation'}>Abbreviation</Button>
                  </ButtonGroup>
                </td>
                <td>
                  <ButtonGroup>
                      <Button color="secondary" onClick={this.setSortSelected.bind(this,'Asc')} active={this.state.orderData.sort === 'Asc'}>Asc</Button>
                      <Button color="secondary" onClick={this.setSortSelected.bind(this,'Desc')} active={this.state.orderData.sort === 'Desc'}>Desc</Button>
                    </ButtonGroup>
                </td>
              </tr>
              <tr key="columns" >
                <th>Name</th>
                <th>Abbreviation</th>
                <th>Make</th>
              </tr>
          </thead>
          <tbody>
            {modelList}
          </tbody>
        </Table>
        <ButtonGroup>
          {pagesButtons}
        </ButtonGroup>
        </div>);
        }
    }
}
 
export default Models;