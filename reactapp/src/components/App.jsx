import React, { Component } from 'react';
import Models from './Models.jsx';
import Makes from './Makes.jsx';
import { Button, ButtonGroup } from 'reactstrap';

//First version of react frontend without mobx.
class App extends Component {
    state = {  
        makesComponentState : false,
        modelsComponentState: false
    }

    toggleMakes(){
        this.setState({modelsComponentState: false, makesComponentState: !this.state.makesComponentState});
    }

    toggleModels(){
        this.setState({modelsComponentState: !this.state.modelsComponentState, makesComponentState: false});
    }

    toggleAllOff(){
        this.setState({modelsComponentState: false, makesComponentState: false})
    }
    render(){
        let component = null;
        if(this.state.makesComponentState === true){
            component =<Makes></Makes>;
        }
        else if(this.state.modelsComponentState === true){
            component =<Models></Models>
        }
        return (
            <div>
                <ButtonGroup style={{textAlign: "center"}}>
                    <Button color="primary" onClick={this.toggleAllOff.bind(this)} active={this.state.modelsComponentState === false && this.state.makesComponentState === false}>Home</Button>
                    <Button color="primary" onClick={this.toggleMakes.bind(this)} active={this.state.makesComponentState === true}>Makes</Button>
                    <Button color="primary" onClick={this.toggleModels.bind(this)} active={this.state.modelsComponentState === true}>Models</Button>
                </ButtonGroup>
                <div>
                    {component}
                </div>
            </div>
        )
    }
}

export default App;