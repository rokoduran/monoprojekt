import React, { Component } from 'react';
import axios from 'axios';
import { Modal, ModalHeader, ModalBody, ModalFooter, Table, Button, FormGroup, Label, Input, ButtonGroup } from 'reactstrap';

//First version of react frontend without mobx.
class Makes extends Component {
    state = {  
        responseData: null,
        newModelPrompt: false,
        newMakePrompt: false,
        editMakePrompt: false,
        
        pagingData: {
          currentPage: 1,
          totalPages: 1
        },

        filterData:{
          nameFilter : '',
          abbrvFilter:'',
          makeNameFilter: ''
        },

        orderData:{
          sort: 'Asc',
          orderBy: 'Name'
        },

        newModelData:{
            id: 0,
            name: '',
            abbreviation: '',
            makeId: 0
        },
        makeData:{
            id: 0,
            name: '',
            abbreviation: ''
        },
    }

    toggleNewModelPrompt(makeId){
        this.state.newModelData.makeId = makeId;
        this.setState({newModelPrompt:!this.state.newModelPrompt});
    }

    toggleEditMakePrompt(id){
        let data = this.state.makeData;
        data.id = id;
        this.setState({makeData: data, editMakePrompt:!this.state.editMakePrompt});
    }

    toggleNewMakePrompt(){
        this.setState({newMakePrompt:!this.state.newMakePrompt});
    }

    changePage(newPageNumber){
      let page = this.state.pagingData;
      page.currentPage = newPageNumber;
      this.setState({pagingData: page});
      this.updateMakeList();
    }

    async fillNameFilter(value){
      let filter = this.state.filterData;
      filter.nameFilter = value;
      this.setState({filterData: filter});
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateMakeList();
      }
    }

    async fillAbbvFilter(value){
      let filter = this.state.filterData;
      filter.abbrvFilter = value;
      this.setState({filterData: filter});
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateMakeList();
      }
    }

    async setOrderSelected(value){
      let order = this.state.orderData;
      order.orderBy = value;
      this.setState({orderData: order });
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateMakeList();
      }
    }
    
    async setSortSelected(value){
      let sort = this.state.orderData;
      sort.sort = value;
      this.setState({orderData: sort});
      if(this.state.pagingData.currentPage!=1){
        this.changePage(1);
      }
      else{
        this.updateMakeList();
      }
    }

    async updateMakeList(){
      let data = {
          "param1": this.state.filterData,
          "param2": this.state.orderData,
          "param3": this.state.pagingData
      }
        await axios.post('https://localhost:44321/api/make/index', data).then(response => {
            this.setState({responseData: response});
            let paging = this.state.pagingData;
            paging.totalPages = response.data.pagingInfo.totalPages;
            paging.currentPage = response.data.pagingInfo.currentPage;
            this.setState({pagingData: paging});
        })
        .catch(err => {
            console.log(err);
        });
    }

    async submitNewModel(){
        await axios.post('https://localhost:44321/api/model/create', this.state.newModelData).then(response =>{
            this.toggleNewModelPrompt(0);
        })
    }

    async submitMake(type) {
        if(type == true){
            await axios.post('https://localhost:44321/api/make/create', this.state.makeData).then(response => {
                this.toggleNewMakePrompt();
            })
            .catch(err => {
                console.log(err);
            });
        }
        else {
            await axios.put('https://localhost:44321/api/make/update', this.state.makeData).then(response => {
                this.toggleEditMakePrompt(0);
            });
        }
        this.updateMakeList();

    };

    async submitDeletionMake(makeId){
        await axios.delete('https://localhost:44321/api/make/delete/' + makeId).then(response =>{
            this.updateMakeList();
        });
    };

    componentDidMount() {
        this._asyncRequest = () => {
            this._asyncRequest = null;
        };
        this.updateMakeList();
      }

    render() { 
        if(this.state.responseData == null){
            return(<div className="container">Fetching data...</div>);
        }
        else{
            let makeList =this.state.responseData.data.makeData.map(make => {
                return(              
                    <tr key={make.id}>
                      <td>{make.name}</td>
                      <td>{make.abbreviation}</td>
                      <td>
                        <Button color="dark" size="sm" className="mx-1" onClick={this.toggleNewModelPrompt.bind(this, make.id)}>Add model</Button> 
                        <Button color="success" size="sm" className="mx-1" onClick={this.toggleEditMakePrompt.bind(this,make.id)}>Edit</Button>
                        <Button color="danger" size="sm" className="mx-1" onClick={this.submitDeletionMake.bind(this, make.id)}>Delete</Button>
                      </td>
                    </tr>
                  )
            })
            let pagesButtons = [];
            for(let i = 0; i < this.state.pagingData.totalPages; i++){
              pagesButtons.push(
                <Button key={i+1} color="secondary" onClick={this.changePage.bind(this,i+1)} active={this.state.pagingData.currentPage === i+1}>{i+1}</Button>
              )
            }
        
    return (  
    <div className="App container">  
        <div>
            <Button className="m-3" color="primary" onClick={this.toggleNewMakePrompt.bind(this)}>New make</Button>
            <Modal isOpen = {this.state.newMakePrompt} toggle={this.toggleNewMakePrompt.bind(this)}>
              <ModalHeader toggle={this.toggleNewMakePrompt.bind(this)}>Add a new make</ModalHeader>
              <ModalBody>
                <FormGroup>
                  <Label for="makeName">Name</Label>
                  <Input type="text" id="makeName"  onChange={(ele) => {
                    this.state.makeData.name = ele.target.value;
                    }
                  } />
                </FormGroup>                
                <FormGroup>
                  <Label for="makeAbbreviation">Abbreviation</Label>
                  <Input type="text" id="makeAbbreviation"  onChange={(ele) => {
                    this.state.makeData.abbreviation = ele.target.value;
                    }} />
                </FormGroup>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.submitMake.bind(this, true)}>Submit</Button>{' '}
                <Button color="secondary" onClick={this.toggleNewMakePrompt.bind(this)}>Cancel</Button>
              </ModalFooter>
            </Modal>
          </div>

          <div>
            <Modal isOpen = {this.state.newModelPrompt} toggle={this.toggleNewModelPrompt.bind(this)}>
              <ModalHeader toggle={this.toggleNewModelPrompt.bind(this)}>Add a model</ModalHeader>
              <ModalBody>

                <FormGroup>
                  <Label for="modelName">Name</Label>
                  <Input type="text" id="modelName"  onChange={(ele) => {
                    this.state.newModelData.name = ele.target.value;
                    }
                  } />
                </FormGroup>                
                <FormGroup>
                  <Label for="modelAbbreviation">Abbreviation</Label>
                  <Input type="text" id="modelAbbreviation"  onChange={(ele) => {
                    this.state.newModelData.abbreviation = ele.target.value;
                    }} />
                </FormGroup>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.submitNewModel.bind(this)}>Submit</Button>{' '}
                <Button color="secondary" onClick={this.toggleNewModelPrompt.bind(this,0)}>Cancel</Button>
              </ModalFooter>
            </Modal>
          </div>

          <div>
            <Modal isOpen = {this.state.editMakePrompt} toggle={this.toggleEditMakePrompt.bind(this, 0)}>
              <ModalHeader toggle={this.toggleEditMakePrompt.bind(this)}>Editing make</ModalHeader>
              <ModalBody>

                <FormGroup>
                  <Label for="editMakeName">Name</Label>
                  <Input type="text" id="editMakeName" onChange={(ele) => {
                    this.state.makeData.name = ele.target.value;
                    }
                  } />
                </FormGroup>                
                <FormGroup>
                  <Label for="editMakeAbbreviation">Abbreviation</Label>
                  <Input type="text" id="editMakeAbbreviation" onChange={(ele) => {
                    this.state.makeData.abbreviation = ele.target.value;
                    }} />
                </FormGroup>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.submitMake.bind(this, false)}>Submit</Button>{' '}
                <Button color="secondary" onClick={this.toggleEditMakePrompt.bind(this,0)}>Cancel</Button>
              </ModalFooter>
            </Modal>
          </div>
        <Table striped bordered hover variant="dark" size="sm">
          <thead variant="dark">
              <tr key="searchParam">
                <td>Search by name</td>
                <td>Search by abbreviation</td>
                <td>Order by</td>
                <td>Sort type</td>
              </tr>
              <tr key="searchBoxes">
                <td>
                  <Input type="text" id="searchNameInput" onChange = {(ele) => this.fillNameFilter(ele.target.value)}/>
                </td>
                <td>
                  <Input type="text" id="searchAbbvInput" onChange = {(ele) => this.fillAbbvFilter(ele.target.value)}/>
                </td>
                <td>
                  <ButtonGroup>
                    <Button color="primary" onClick={this.setOrderSelected.bind(this,'Name')} active={this.state.orderData.orderBy === 'Name'}>Name</Button>
                    <Button color="primary" onClick={this.setOrderSelected.bind(this,'Abbreviation')} active={this.state.orderData.orderBy === 'Abbreviation'}>Abbreviation</Button>
                  </ButtonGroup>
                </td>
                <td>
                  <ButtonGroup>
                      <Button color="secondary" onClick={this.setSortSelected.bind(this,'Asc')} active={this.state.orderData.sort === 'Asc'}>Asc</Button>
                      <Button color="secondary" onClick={this.setSortSelected.bind(this,'Desc')} active={this.state.orderData.sort === 'Desc'}>Desc</Button>
                    </ButtonGroup>
                </td>
              </tr>
              <tr key="columns" >
                <th>Name</th>
                <th>Abbreviation</th>
              </tr>
          </thead>
          <tbody>
            {makeList}
          </tbody>
        </Table>
        <ButtonGroup>
          {pagesButtons}
        </ButtonGroup>
        </div>);
        }
    }
}
 
export default Makes;