import React, { Component } from 'react';
import  MakeStoreImpl  from '../stores/MakeStore'
import { observer, useObserver } from 'mobx-react';

//const makeStore = new MakeStore();

const MakesComponent = ({MakeStoreImpl}) =>
    {
    MakeStoreImpl.makeIndex();
    return useObserver(() =>
        <span>{this.props.MakeStoreImpl.makeList}</span>
        )
    }

export default MakesComponent;