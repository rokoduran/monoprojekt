﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Model.Common;

namespace Model.Domain
{
    public class VehicleMake : IVehicleMake
    {
        public int ID { get; set; }
        [Required, StringLength(40)]public string Name { get; set; }
        [Required, StringLength(10)]public string Abbreviation { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public IList<IVehicleModel> VehicleModels { get; set; }
    }
}