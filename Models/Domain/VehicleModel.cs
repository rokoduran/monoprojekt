﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Model.Common;

namespace Model.Domain
{
    public class VehicleModel: IVehicleModel
    {
        [Required, StringLength(40)] public string Name { get; set; }
        [Required, StringLength(10)] public string Abbreviation { get; set; }

        [Required] public int MakeId { get; set; }
        public IVehicleMake Make { get; set; }

        [ScaffoldColumn(false)] public int ID { get; set; }

        [Required] public DateTime DateCreated { get; set; }
        [Required] public DateTime DateUpdated { get; set; }
    }
}
