﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Common;
using Model.Domain;
using Ninject.Modules;

namespace Model.Bindings
{
    public class ModelBinding : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IVehicleMake>().To<VehicleMake>();
            this.Bind<IVehicleModel>().To<VehicleModel>();
        }
    }
}
