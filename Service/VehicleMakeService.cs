﻿using Model.Common;
using Repository.Common;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class VehicleMakeService : BaseService<IVehicleMakeRepository, IVehicleMake>, IVehicleMakeService
    {
        public VehicleMakeService(IVehicleMakeRepository repository) : base(repository)
        {
        }
    }
}
