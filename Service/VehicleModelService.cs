﻿using Model.Common;
using Repository.Common;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class VehicleModelService: BaseService<IVehicleModelRepository, IVehicleModel>, IVehicleModelService
    {
        public VehicleModelService(IVehicleModelRepository repository): base(repository)
        {
        }
    }
}
