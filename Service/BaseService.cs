﻿using FeatureParams;
using Model.Common;
using Repository.Common;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public abstract class BaseService<TRepository, TType> : IGenericService<TType>
        where TType : class, IBase
        where TRepository : class, IGenericRepository<TType>
    {
        protected readonly TRepository Repository;

        public BaseService(TRepository repository) => Repository = repository;

        public async Task<bool> DeleteAsync(int id)
        {
            return await Repository.DeleteAsync(id);
        }

        public async Task<IEnumerable<TType>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort)
        {
           return (await Repository.GetAllForParamsAsync(filter, paging, sort));
        }

        public async Task<TType> GetByIDAsync(int id)
        {
            return await Repository.GetByIDAsync(id);
        }

        public async Task<int> GetCountAsync(FilteringParam filter)
        {
            return await Repository.GetCountAsync(filter);
        }

        public async Task<TType> InsertAsync(TType insert)
        {
            insert.DateCreated = System.DateTime.Now;
            insert.DateUpdated = System.DateTime.Now;
            return await Repository.InsertAsync(insert);

        }

        public async Task SaveChangesAsync()
        {
            await Repository.SaveChangesAsync();
        }

        public async Task<TType> UpdateAsync(TType update)
        {
            return await Repository.UpdateAsync(update);
        }
    }
}
