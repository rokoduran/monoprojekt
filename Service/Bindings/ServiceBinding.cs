﻿using Ninject.Modules;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Bindings
{
    public class ServiceBinding : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IVehicleModelService>().To<VehicleModelService>();
            this.Bind<IVehicleMakeService>().To<VehicleMakeService>();
        }
    }
}
