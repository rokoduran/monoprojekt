﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.ViewModels
{
    public class ViewVehicleMake
    {
        public int ID { get; set; }
        [Required, StringLength(40)] public string Name { get; set; }
        [Required, StringLength(10)] public string Abbreviation { get; set; }
    }
}
