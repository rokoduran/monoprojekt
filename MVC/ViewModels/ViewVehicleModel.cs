﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.ViewModels
{
    public class ViewVehicleModel
    {
        [Required, StringLength(40)] public string Name { get; set; }
        [Required, StringLength(10)] public string Abbreviation { get; set; }
        [Required] public int MakeId { get; set; }
        public string MakeName { get; set; }
        [ScaffoldColumn(false)] public int ID { get; set; }
    }
}
