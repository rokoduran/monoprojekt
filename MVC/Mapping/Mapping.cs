﻿using AutoMapper;
using DataAccessLayer.DatabaseModels;
using Model.Common;
using Model.Domain;
using MVC.ViewModels;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Mapping
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<VehicleMakeEntity, VehicleMake>().ReverseMap();
            CreateMap<VehicleMakeEntity, IVehicleMake>().ReverseMap();
            CreateMap<VehicleModelEntity, IVehicleModel>().ReverseMap();
            CreateMap<VehicleModelEntity, VehicleModel>().ReverseMap();
            CreateMap<VehicleMake, ViewVehicleMake>().ReverseMap();
            CreateMap<VehicleModel, ViewVehicleModel>().ForMember(dest =>
                dest.MakeName, opt => opt.MapFrom(src => src.Make.Name));
            CreateMap<ViewVehicleModel, VehicleModel>();
        }
    }

    public class AutoMapperModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(AutoMapper).InSingletonScope();
        }

        private IMapper AutoMapper(Ninject.Activation.IContext context)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new Mapping());
            });
            var mapper = mapperConfig.CreateMapper();
            return mapper;
        }
    }
}
