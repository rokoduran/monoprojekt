﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FeatureParams;
using Microsoft.AspNetCore.Mvc;
using Model.Domain;
using MVC.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service.Common;

namespace MVC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelController : ControllerBase
    {
        protected readonly IVehicleModelService m_modelService;
        protected readonly IMapper m_mapper;

        public ModelController(IVehicleModelService ModelService, IMapper Mapper)
        {
            m_modelService = ModelService;
            m_mapper = Mapper;
        }

        [HttpPost]
        [Route("index")]
        public async Task<IActionResult> Index([FromBody]object obj)
        {
            JObject o = JObject.Parse(obj.ToString());

            string[] str = new string[o.Count];

            for (int i = 0; i < o.Count; i++)
            {
                string var = "param" + (i + 1).ToString();
                str[i] = o[var].ToString();
            }

            FilteringParam filter = JsonConvert.DeserializeObject<FilteringParam>(str[0]);
            SortingParam sort = JsonConvert.DeserializeObject<SortingParam>(str[1]);
            PagingParam paging = JsonConvert.DeserializeObject<PagingParam>(str[2]);

            if (!(sort.OrderBy == SortingParam.OrderBySettings.Item1 || sort.OrderBy == SortingParam.OrderBySettings.Item2))
                return BadRequest("Wrong order by parameters.");

            if (!(sort.Sort == SortingParam.SortSettings.Item1 || sort.Sort == SortingParam.SortSettings.Item2))
                return BadRequest("Wrong sort paramters.");

            var modelList = m_mapper.Map<List<ViewVehicleModel>>(await m_modelService.GetAllForParamsAsync(filter, paging, sort));
            var count = await m_modelService.GetCountAsync(filter);
            var pagingData = new
            {
                CurrentPage = paging.currentPage,
                TotalPages = (int)Math.Ceiling(count / (double)PagingParam.PageSize)
            };

            var result = new { pagingInfo = pagingData, modelData = modelList };
            return Ok(result);
        }

        [HttpGet]
        [Route("get/{modelId}")]
        public async Task<IActionResult> Get(int modelId)
        {
            var model = await m_modelService.GetByIDAsync(modelId);

            if (model == null)
            {
                return NotFound("Model with requested ID does not exist");
            }

            return Ok(m_mapper.Map<ViewVehicleModel>(model));
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(ViewVehicleModel newModelView)
        {
            var newModel = m_mapper.Map<VehicleModel>(newModelView);
            var result = m_mapper.Map<ViewVehicleModel>(await m_modelService.InsertAsync(newModel));
            if(result == null)
            {
                return BadRequest("Something was wrong in the provided model details.");
            }
            return Ok(result);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update(ViewVehicleModel modelToUpdateView)
        {
            var originalModel = await m_modelService.GetByIDAsync(modelToUpdateView.ID);
            if (originalModel == null)
            {
                return NotFound("Invalid model to update");
            }

            var modelToUpdate = m_mapper.Map<VehicleModel>(modelToUpdateView);

            modelToUpdate.MakeId = originalModel.MakeId;
            modelToUpdate.DateCreated = originalModel.DateCreated;

            var result = m_mapper.Map<ViewVehicleModel>(await m_modelService.UpdateAsync(modelToUpdate));
            return Ok(result);

        }

        [HttpDelete]
        [Route("delete/{modelId}")]
        public async Task<IActionResult> Delete(int modelId)
        {
            var result = await m_modelService.DeleteAsync(modelId);
            if (!result)
            {
                return NotFound("Invalid modelId");
            }
            return Ok();
        }
    }
}
