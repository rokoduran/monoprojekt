﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using FeatureParams;
using Microsoft.AspNetCore.Mvc;
using Model.Domain;
using MVC.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service.Common;

namespace MonoProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MakeController : ControllerBase
    {
        protected readonly IVehicleMakeService m_makeService;
        protected readonly IMapper m_mapper;

        public MakeController(IVehicleMakeService makeService, IMapper mapper)
        {
            m_makeService = makeService;
            m_mapper = mapper;
        }

        [HttpPost]
        [Route("index")]
        public async Task<IActionResult> Index([FromBody]object obj)
        {
            JObject o = JObject.Parse(obj.ToString());

            string[] str = new string[o.Count];

            for (int i = 0; i < o.Count; i++)
            {
                string var = "param" + (i + 1).ToString();
                str[i] = o[var].ToString();
            }

            FilteringParam filter = JsonConvert.DeserializeObject<FilteringParam>(str[0]);
            SortingParam sort = JsonConvert.DeserializeObject<SortingParam>(str[1]);
            PagingParam paging = JsonConvert.DeserializeObject<PagingParam>(str[2]);

            if (!(sort.OrderBy == SortingParam.OrderBySettings.Item1 || sort.OrderBy == SortingParam.OrderBySettings.Item2))
                return BadRequest("Wrong order by parameters.");

            if (!(sort.Sort == SortingParam.SortSettings.Item1 || sort.Sort == SortingParam.SortSettings.Item2))
                return BadRequest("Wrong sort paramters.");

            var makeList = m_mapper.Map<List<ViewVehicleMake>>(await m_makeService.GetAllForParamsAsync(filter, paging, sort));

            int count = await m_makeService.GetCountAsync(filter);

            var pagingData = new
            {
                CurrentPage = paging.currentPage,
                TotalPages = (int)Math.Ceiling(count / (double)PagingParam.PageSize)
            };
            var result = new { pagingInfo = pagingData, makeData = makeList };
            return Ok(result);
        }

        [HttpGet]
        [Route ("get/{makeId}")]
        public async Task<IActionResult> Get(int makeId)
        {
            var make = await m_makeService.GetByIDAsync(makeId);

            if (make == null)
            { 
                return NotFound("Make with requested ID does not exist");
            }

            return Ok(m_mapper.Map<ViewVehicleMake>(make));
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(ViewVehicleMake newMakeView)
        {
            var newMake = m_mapper.Map<VehicleMake>(newMakeView);
            var result = m_mapper.Map<ViewVehicleMake>(await m_makeService.InsertAsync(newMake));
            if (result == null)
            {
                return BadRequest("Invalid data for creating new make");
            }
            return Ok(result);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update(ViewVehicleMake makeToUpdateView)
        {
            var originalMake = await m_makeService.GetByIDAsync(makeToUpdateView.ID);
            if (originalMake == null)
            {
                return NotFound("Invalid make to update");
            }

            var makeToUpdate = m_mapper.Map<VehicleMake>(makeToUpdateView);

            makeToUpdate.DateCreated = originalMake.DateCreated;

            var result = m_mapper.Map<ViewVehicleMake>(await m_makeService.UpdateAsync(makeToUpdate));
            return Ok(result);

        }

        [HttpDelete]
        [Route("delete/{makeId}")]
        public async Task<IActionResult> Delete(int makeId)
        {
            var result = await m_makeService.DeleteAsync(makeId);
            if (!result)
            {
                return NotFound("Invalid makeId");
            }
                return Ok();
        }
    }
}