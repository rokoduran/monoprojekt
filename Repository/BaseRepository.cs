﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Repository.Bindings;
using DataAccessLayer.Context;
using Model.Common;
using Repository.Common;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using FeatureParams;

namespace Repository
{
    public abstract class BaseRepository<TInterface, TDomain, TData> : IGenericRepository<TInterface>
        where TInterface : class
        where TDomain : class, TInterface
        where TData : class, IBase

    {
        protected readonly VehicleContext dbContext;
        protected readonly IMapper mapper;

        protected BaseRepository(VehicleContext DbContext, IMapper Mapper)
        {
            dbContext = DbContext;
            mapper = Mapper;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var e = await dbContext.Set<TData>().FindAsync(id);
            if (e != null)
            {
                dbContext.Set<TData>().Attach(e);
                dbContext.Entry(e).State = EntityState.Deleted;
                await SaveChangesAsync();
                return true;
            }
            return false;
        }
        //Override in derived repos. Can only work with paging as it's unknown which specific data set is in question for other params.
        public virtual async Task<IEnumerable<TInterface>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort)
        {
            var query = dbContext.Set<TData>().AsQueryable();
            return mapper.Map<List<TDomain>>(await query.Skip((paging.currentPage - 1) * PagingParam.PageSize).Take(PagingParam.PageSize).ToListAsync());
        }

        public async Task<TInterface> GetByIDAsync(int id)
        {
            return mapper.Map<TDomain>(await dbContext.Set<TData>().AsNoTracking().SingleOrDefaultAsync(x => x.ID == id));
        }

        public virtual async Task<int> GetCountAsync(FilteringParam param)
        {
            return mapper.Map<List<TDomain>>(await dbContext.Set<TData>().ToListAsync()).Count();
        }

        public virtual async Task<TInterface> InsertAsync(TInterface insert)
        {
            var newEntry = dbContext.Set<TData>().Add(mapper.Map<TData>(insert));

            await SaveChangesAsync();
            return mapper.Map<TDomain>(newEntry.Entity);
        }

        public async Task SaveChangesAsync()
        {
            await dbContext.SaveChangesAsync();
        }

        public async Task<TInterface> UpdateAsync(TInterface update)
        {
            var entity = mapper.Map<TData>(update);
            entity.DateUpdated = DateTime.Now;
            TData existing = dbContext.Set<TData>().Find(entity.ID);

            if (existing != null)
            {
                dbContext.Entry(existing).CurrentValues.SetValues(entity);
                await SaveChangesAsync();
            }
            return mapper.Map<TDomain>(existing);
        }
    }
}
