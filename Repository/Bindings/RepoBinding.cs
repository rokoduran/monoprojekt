﻿using System;
using System.Collections.Generic;
using System.Text;
using Ninject.Modules;
using DataAccessLayer;
using Model;
using Model.Common;
using Repository.Common;
using Model.Domain;
using DataAccessLayer.DatabaseModels;
using DataAccessLayer.Context;

namespace Repository.Bindings
{
    public class RepoBinding: NinjectModule
    {
        public override void Load()
        {
            this.Bind<IGenericRepository<IVehicleMake>>().To<IVehicleMakeRepository>();
            this.Bind<IGenericRepository<IVehicleModel>>().To<IVehicleModelRepository>();

            this.Bind<BaseRepository<IVehicleMake, VehicleMake, VehicleMakeEntity>, IVehicleMakeRepository>().To<VehicleMakeRepository>();
            this.Bind<BaseRepository<IVehicleModel, VehicleModel, VehicleModelEntity>, IVehicleModelRepository>().To<VehicleModelRepository>();
            this.Bind<VehicleContext>().ToSelf().InSingletonScope();
        }
    }
}
