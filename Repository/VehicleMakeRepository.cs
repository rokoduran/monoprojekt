﻿using AutoMapper;
using DataAccessLayer.Context;
using DataAccessLayer.DatabaseModels;
using FeatureParams;
using Microsoft.EntityFrameworkCore;
using Model.Common;
using Model.Domain;
using Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class VehicleMakeRepository : BaseRepository<IVehicleMake, VehicleMake, VehicleMakeEntity>, IVehicleMakeRepository
    {
        public VehicleMakeRepository(VehicleContext dbContext, IMapper mapper): base(dbContext, mapper)
        {
        }

        protected IQueryable<VehicleMakeEntity> GetByFilter(FilteringParam filter)
        {
            var query = dbContext.Set<VehicleMakeEntity>().AsQueryable();

            if (filter.NameFilter != "")
            {
                query = query.Where(x => x.Name.ToLower().Contains(filter.NameFilter.ToLower()));
            }
            if (filter.AbbrvFilter != "")
            {
                query = query.Where(x => x.Abbreviation.ToLower().Contains(filter.AbbrvFilter.ToLower()));
            }
            return query;
        }

        public override async Task<int> GetCountAsync(FilteringParam filter)
        {
            if(filter == null)
            {
                filter = new FilteringParam();
            }
            return (await GetByFilter(filter).ToListAsync()).Count();
        }

        public override async Task<IEnumerable<IVehicleMake>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort)
        {
            //Filtering
            var query = GetByFilter(filter);
            //Sorting
            bool reverseOrder = sort.Sort == SortingParam.SortSettings.Item2;
            if(sort.OrderBy == SortingParam.OrderBySettings.Item1)
            {
                query = reverseOrder ? query.OrderByDescending(x => x.Name) : query.OrderBy(x => x.Name);
            }
            else if(sort.OrderBy == SortingParam.OrderBySettings.Item2)
            {
                query = reverseOrder? query.OrderByDescending(x => x.Abbreviation): query.OrderBy(x => x.Abbreviation);
            }

            //Paging and end
            var makeList = (await query.Skip((paging.currentPage - 1) * PagingParam.PageSize).Take(PagingParam.PageSize).ToListAsync());

            return mapper.Map<List<VehicleMake>>(makeList);
        }
    }
}
