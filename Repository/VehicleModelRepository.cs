﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer.Context;
using DataAccessLayer.DatabaseModels;
using FeatureParams;
using Microsoft.EntityFrameworkCore;
using Model.Common;
using Model.Domain;
using Repository.Common;

namespace Repository
{
    public class VehicleModelRepository : BaseRepository<IVehicleModel, VehicleModel, VehicleModelEntity>, IVehicleModelRepository
    {
        public VehicleModelRepository(VehicleContext dbContext, IMapper mapper): base(dbContext, mapper)
        {
        }

        public override async Task<IVehicleModel> InsertAsync(IVehicleModel insert)
        {
            var make = mapper.Map<VehicleMake>(await dbContext.Set<VehicleMakeEntity>().AsNoTracking().SingleOrDefaultAsync(x => x.ID == insert.MakeId));
            if(make == null)
            {
                return null;
            }
            var newEntry = dbContext.Set<VehicleModelEntity>().Add(mapper.Map<VehicleModelEntity>(insert));

            await SaveChangesAsync();
            return mapper.Map<VehicleModel>(newEntry.Entity);
        }

        protected IQueryable<VehicleModelEntity> GetByFilter(FilteringParam filter)
        {
            var query = dbContext.Set<VehicleModelEntity>().Include(x=> x.Make).AsQueryable();

            if (filter.NameFilter != "")
            {
                query = query.Where(x => x.Name.ToLower().Contains(filter.NameFilter.ToLower()));
            }
            if (filter.AbbrvFilter != "")
            {
                query = query.Where(x => x.Abbreviation.ToLower().Contains(filter.AbbrvFilter.ToLower()));
            }
            if(filter.MakeNameFilter !="")
            {
                query = query.Where(x => x.Make.Name.ToLower().Contains(filter.MakeNameFilter.ToLower()));
            }
            return query;
        }

        public override async Task<int> GetCountAsync(FilteringParam filter)
        {
            if (filter == null)
            {
                filter = new FilteringParam();
            }
            return (await GetByFilter(filter).ToListAsync()).Count();
        }

        public override async Task<IEnumerable<IVehicleModel>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort)
        {
            //Filtering
            var query = GetByFilter(filter);
            //Sorting
            bool reverseOrder = sort.Sort == SortingParam.SortSettings.Item2;
            if (sort.OrderBy == SortingParam.OrderBySettings.Item1)
            {
                query = reverseOrder ? query.OrderByDescending(x => x.Name) : query.OrderBy(x => x.Name);
            }
            else if (sort.OrderBy == SortingParam.OrderBySettings.Item2)
            {
                query = reverseOrder ? query.OrderByDescending(x => x.Abbreviation) : query.OrderBy(x => x.Abbreviation);
            }

            //Paging and end;
            var modelList = (await query.Skip((paging.currentPage - 1) * PagingParam.PageSize).Take(PagingParam.PageSize).ToListAsync());

            return mapper.Map<List<VehicleModel>>(modelList);
        }
    }
}